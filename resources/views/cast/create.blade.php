
@extends('layouts.master')

@section('judul')
    Halaman tambah cast
@endsection
@section('title')
    Halaman tambah cast
@endsection

@section('conten')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>nama cast</label>
      <input name="nama" type="text" class="form-control" >

    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label>umur cast</label>
<textarea name="umur"  cols="30" rows="10" class="form-control"></textarea>    
</div>
@error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label>bio cast</label>
<textarea name="bio"  cols="30" rows="10" class="form-control"></textarea>    
</div>
@error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection