
@extends('layouts.master')

@section('judul')
    Halaman tambah cast
@endsection
@section('title')
    Halaman tambah cast
@endsection

@section('conten')
<form action="/cast/{{$cast ->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>nama cast</label>
      <input name="nama"value="{{$cast ->nama}}" type="text" class="form-control" >

    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label>umur cast</label>
<textarea name="umur"  cols="30" rows="10" class="form-control">{{$cast ->umur}}</textarea>    
</div>
@error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label>bio cast</label>
<textarea name="bio"  cols="30" rows="10" class="form-control">{{$cast ->bio}}</textarea>    
</div>
@error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection