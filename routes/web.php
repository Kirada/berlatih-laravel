<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@utama');
Route::get('/register', 'AuthController@daftar');
Route::post('/welcome', 'AuthController@kirim');

Route:: get('/master',function(){

    return view ('layouts.master');
});

Route:: get('/table',function(){

    return view ('layouts.table');
});

Route:: get('/data-table',function(){

    return view ('layouts.datatable');
});

//crud cast


Route:: get('/cast/create', 'CastController@create');
Route:: post('/cast', 'CastController@store');

//read data cast
Route:: get('/cast', 'CastController@index');
Route:: get('/cast/{cast_id}', 'CastController@show');

//update data cast
Route:: get('/cast/{cast_id}/edit','CastController@edit');
Route:: put('/cast/{cast_id}','CastController@update');
//delet data cast

Route::delete('/cast/{cast_id}','CastController@destroy');